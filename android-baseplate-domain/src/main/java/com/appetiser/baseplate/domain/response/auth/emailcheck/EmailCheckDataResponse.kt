package com.appetiser.mycodingchallenge.domain.response.auth.emailcheck

import com.appetiser.mycodingchallenge.domain.response.BaseResponse


data class EmailCheckDataResponse(val data: EmailExistDataResponse) : BaseResponse()
