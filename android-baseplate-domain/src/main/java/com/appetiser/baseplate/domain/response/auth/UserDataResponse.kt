package com.appetiser.mycodingchallenge.domain.response.auth

import com.appetiser.mycodingchallenge.domain.models.APIUser

open class UserDataResponse(
        val user: APIUser,
        val token: String = ""
)
