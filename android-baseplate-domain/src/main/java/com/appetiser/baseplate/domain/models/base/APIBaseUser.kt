package com.appetiser.mycodingchallenge.domain.models.base

import com.google.gson.annotations.SerializedName


open class APIBaseUser(
        @field:SerializedName("last_name")
        open val lastName: String? = "",
        @field:SerializedName("first_name")
        open val firstName: String? = "",
        @field:SerializedName("full_name")
        open val fullName: String? = "",
        open val email: String = "",
        @field:SerializedName("profile_photo_url")
        open val photoUrl: String? = "",
        @field:SerializedName("id")
        open val uid: String = ""
)
