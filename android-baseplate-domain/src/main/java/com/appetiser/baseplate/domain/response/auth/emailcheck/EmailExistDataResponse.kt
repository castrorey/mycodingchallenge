package com.appetiser.mycodingchallenge.domain.response.auth.emailcheck

import com.google.gson.annotations.SerializedName

data class EmailExistDataResponse(@field:SerializedName("email_exists") val isEmailExists: Boolean)
