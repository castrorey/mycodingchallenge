package com.appetiser.mycodingchallenge.domain.response.country_codes

import com.appetiser.mycodingchallenge.domain.response.BaseResponse

data class CountryCodeResponse(val data: CountryCodeDataResponse) : BaseResponse()
