package com.appetiser.mycodingchallenge.domain.response.country_codes

import com.appetiser.mycodingchallenge.domain.models.APICountryCode

open class CountryCodeDataResponse(
        val countries: List<APICountryCode>
)
