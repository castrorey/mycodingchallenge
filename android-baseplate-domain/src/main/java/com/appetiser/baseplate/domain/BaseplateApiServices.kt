package com.appetiser.mycodingchallenge.domain

import com.appetiser.mycodingchallenge.domain.response.BaseResponse
import com.appetiser.mycodingchallenge.domain.response.auth.AuthDataResponse
import com.appetiser.mycodingchallenge.domain.response.auth.emailcheck.EmailCheckDataResponse
import com.appetiser.mycodingchallenge.domain.response.country_codes.CountryCodeResponse
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.RequestBody
import retrofit2.http.*

interface BaseplateApiServices {


    @FormUrlEncoded
    @POST("auth/register")
    fun registerUser(@FieldMap fields: Map<String, @JvmSuppressWildcards Any>): Single<AuthDataResponse>

    @FormUrlEncoded
    @POST("auth/profile/update")
    fun updateProfileInfo(
            @Header("Authorization") token: String,
            @FieldMap fields: Map<String, @JvmSuppressWildcards Any>
    ): Single<AuthDataResponse>

    @FormUrlEncoded
    @POST("auth/login")
    fun loginUser(@FieldMap fields: Map<String, @JvmSuppressWildcards Any>): Single<AuthDataResponse>

    @FormUrlEncoded
    @POST("auth/email/check")
    fun checkEmailIfExists(@FieldMap fields: Map<String, @JvmSuppressWildcards Any>): Single<EmailCheckDataResponse>

    // TODO error when sending request to server
    @FormUrlEncoded
    @POST("auth/verify")
    fun verifyUser(@FieldMap fields: Map<String, @JvmSuppressWildcards Any>): Single<AuthDataResponse>

    @FormUrlEncoded
    @POST("auth/verification/resend")
    fun resendVerificationCode(@FieldMap fields: Map<String, @JvmSuppressWildcards Any>): Single<BaseResponse>

    @FormUrlEncoded
    @PUT("user/update")
    fun updateUserInfo(
            @Header("Authorization") token: String,
            @FieldMap fields: Map<String, @JvmSuppressWildcards Any>
    ): Observable<BaseResponse>

    // TODO error when sending request to server
    @FormUrlEncoded
    @POST("user/upload")
    fun uploadImage(@Header("Authorization") token: String,
                    @FieldMap fields: Map<String, @JvmSuppressWildcards Any>): Single<BaseResponse>

    /**
     *
     *  Get methods for auth
     *
     * */

    @GET("/auth/user")
    fun getUserInfo(@Header("Authorization") token: String): Single<AuthDataResponse>

    @GET("/auth/logout")
    fun logout(@Header("Authorization") token: String): Single<BaseResponse>


    /**
     *
     *  Password
     *
     * */
    @FormUrlEncoded
    @POST("password/reset")
    fun resetPassword(
            @Header("Authorization") token: String,
            @FieldMap fields: Map<String, @JvmSuppressWildcards Any>
    ): Single<AuthDataResponse>

    @FormUrlEncoded
    @POST("password/email")
    fun sendResetPassword(@FieldMap fields: Map<String, @JvmSuppressWildcards Any>): Single<AuthDataResponse>


    /**
     *
     *  getCountryCodes(@Body body: RequestBody)
     *
     *  sample request body
     *   {
     *   "country_ids": [
     *       "608"
     *       ]
     *   }
     *
     * */
    @POST("countries")
    fun getCountryCodes(@Body body: RequestBody): Single<CountryCodeResponse>

    @POST("countries")
    fun getAllCountryCodes(): Single<CountryCodeResponse>

}
