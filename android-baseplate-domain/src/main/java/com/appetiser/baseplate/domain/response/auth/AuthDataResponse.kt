package com.appetiser.mycodingchallenge.domain.response.auth

import com.appetiser.mycodingchallenge.domain.response.BaseResponse

data class AuthDataResponse(val data: UserDataResponse) : BaseResponse()
