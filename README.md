# MyCodingChallenge

# Objectives:
Create a master-detail application that contains at least one dependency. This application should display a list of items obtained from a iTunes Search API and show a detailed view of each item.


## Persistence:
  - Database
  - Entity Classes - DB models (e.g. DBUser)
  - Dao
  - SharedPreferences
  - _LocalSource interfaces
  
   
 ## Requirements:
  - Android version 21 - 29 
  
 ## Built with:
   1. MVVM Architecture
   2. Repository pattern
   3. RXJava
   4. Retrofit
   5. Dagger 2
   
 ## List of modules and dependency
   1. android-baseplate-domain module
   2. android-baseplate-persistence module
   3. auth_modern module
   4. auth_traditional module
    
