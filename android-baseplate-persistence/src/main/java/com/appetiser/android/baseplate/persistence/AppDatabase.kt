package com.appetiser.android.baseplate.persistence

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.appetiser.android.baseplate.persistence.dao.TokenDao
import com.appetiser.android.baseplate.persistence.dao.UserDao
import com.appetiser.android.baseplate.persistence.model.DBToken
import com.appetiser.android.baseplate.persistence.model.DBUserSession

@Database(
    entities = [
        DBUserSession::class,
        DBToken::class
    ],
    version = 1,
    exportSchema = true
)
@TypeConverters
abstract class AppDatabase : RoomDatabase() {

    abstract fun userSessionDao(): UserDao

    abstract fun tokenDao(): TokenDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context, AppDatabase::class.java, "mycodingchallenge.db")
                .fallbackToDestructiveMigration()
                .build()
    }
}
