package com.appetiser.android.baseplate.persistence.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = DBUserSession.USER_TABLE_NAME)
data class DBUserSession(
        @PrimaryKey(autoGenerate = true)
        var id: Long? = null,
        @ColumnInfo(name = "full_name")
        var fullName: String? = "",
        @ColumnInfo(name = "first_name")
        var firstName: String? = "",
        @ColumnInfo(name = "last_name")
        var lastName: String? = "",
        var email: String? = "",
        @ColumnInfo(name = "profile_photo_url")
        var photoUrl: String? = "",
        @ColumnInfo(name = "email_verified_at")
        var emailVerifiedAt: String? = "",
        @ColumnInfo(name = "dob")
        var dateOfBirth: String? = "",
        var uid: String = "",
        @ColumnInfo(name = "screen_type")
        var screenType: Int? = 0
) {

    companion object {
        const val USER_TABLE_NAME = "user_session"
    }

    constructor() : this(0, "", "", "", "", "", "", "", "")

}
