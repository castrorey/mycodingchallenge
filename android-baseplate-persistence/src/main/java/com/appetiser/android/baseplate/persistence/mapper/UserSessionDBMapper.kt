package com.appetiser.android.baseplate.persistence.mapper


interface UserSessionDBMapper<in DBUser, T> {

     fun mapFromDB(from: DBUser): T
}
