package sampleData

import com.appetiser.mycodingchallenge.data.poko.CountryCode
import com.appetiser.mycodingchallenge.data.poko.Track

class SampleData {

    companion object {
        const val USER_NAME = "francis.cerio@appetiser.com.au"
        const val CORRECT_PASSWORD = "password"
        const val WRONG_PASSWORD = "123456"

        fun stubCountries(): List<CountryCode> {
            return mutableListOf<CountryCode>()
                    .apply {
                        add(CountryCode(1, "PH", "63", "/images/flags/PH.png"))
                        add(CountryCode(2, "AU", "61", "/images/flags/AU.png"))
                    }
        }

        fun stubTracks(): List<Track> {
            return mutableListOf<Track>()
        }
    }

}
