package com.appetiser.mycodingchallenge.ext

import android.text.Editable
import android.text.TextWatcher
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.widget.AppCompatImageButton

inline fun Button.enableWhen(editText: EditText, crossinline predicate: (CharSequence) -> Boolean) {
    editText.addTextChangedListener(object : TextWatcher {

        override fun onTextChanged(input: CharSequence, start: Int, before: Int, count: Int) {
            isEnabled = if (input.isNotEmpty()) {
                predicate(input)
            } else {
                false
            }
        }

        override fun afterTextChanged(s: Editable?) {}

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
    })
    editText.text = editText.text // Force trigger
}

inline fun AppCompatImageButton.enableWhen(editText: EditText, crossinline predicate: (CharSequence) -> Boolean) {
    editText.addTextChangedListener(object : TextWatcher {

        override fun onTextChanged(input: CharSequence, start: Int, before: Int, count: Int) {
            isEnabled = if (input.isNotEmpty()) {
                predicate(input)
            } else {
                false
            }
        }

        override fun afterTextChanged(s: Editable?) {}

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
    })
    editText.text = editText.text // Force trigger
}
