package com.appetiser.mycodingchallenge.data.poko

data class User (val emailAddress: String = "",
                 val password: String = "")
