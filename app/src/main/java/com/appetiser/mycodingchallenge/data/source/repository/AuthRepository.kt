package com.appetiser.mycodingchallenge.data.source.repository

import com.appetiser.mycodingchallenge.data.AuthSource
import com.appetiser.mycodingchallenge.data.mapper.getAccessToken
import com.appetiser.mycodingchallenge.data.mapper.getUserSession
import com.appetiser.mycodingchallenge.data.poko.AccessToken
import com.appetiser.mycodingchallenge.data.poko.UserSession
import com.appetiser.mycodingchallenge.data.source.local.AuthLocalSource
import com.appetiser.mycodingchallenge.data.source.remote.AuthRemoteSource
import com.appetiser.mycodingchallenge.data.poko.CountryCode
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.Function3
import javax.inject.Inject


class AuthRepository @Inject
constructor(private val remote: AuthRemoteSource, private val local: AuthLocalSource) : AuthSource {

    override fun checkEmail(email: String): Single<Boolean> = remote.checkEmail(email)

    override fun saveCredentials(user: UserSession): Single<UserSession> {
        return local.saveCredentials(user)
    }

    override fun saveToken(token: String): Single<AccessToken> {
        return local.saveToken(token)
    }

    override fun login(email: String, password: String): Single<Map<String, Any>> {
        return remote.login(email, password)
                .flatMap {
                    saveUserCredentialsAndToken(it).singleOrError()
                }
    }

    override fun register(email: String, password: String, confirmPassword: String, country: String, dob: String, mobileNumber: String, firstName: String, lastName: String, address: String): Single<Map<String, Any>> {
        return remote.register(email, password, confirmPassword, country, dob, mobileNumber, firstName, lastName, address)
                .flatMap {
                    saveUserCredentialsAndToken(it).singleOrError()
                }
    }

    override fun getUserSession(): Single<UserSession> {
        return local.getUserSession()
                .onErrorResumeNext(local.getUserSession())
    }

    override fun verifyAccount(uid: String, code: String): Single<Boolean> = remote.verifyAccount(uid, code)

    override fun resendVerificationCode(email: String): Single<Boolean> {
        return remote.resendVerificationCode(email)
    }

    private fun saveUserCredentialsAndToken(userSessionMapper: Map<String, Any>): Observable<Map<String, Any>> {
        val userSession = userSessionMapper.getUserSession()
        val accesstoken = userSessionMapper.getAccessToken()

        return Observable.zip(Observable.just(userSessionMapper), saveCredentials(userSession)
                .toObservable(), saveToken(accesstoken.token.orEmpty())
                .toObservable(),
                Function3 { mapper, _, _ ->
                    return@Function3 mapper
                })
    }

    override fun forgotPassword(email: String): Single<Boolean> {
        return remote.forgotPassword(email)
    }

    override fun logout(): Completable {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getCountryCode(countryCodes: List<Int>): Single<List<CountryCode>> = remote.getCountryCode(countryCodes)

}
