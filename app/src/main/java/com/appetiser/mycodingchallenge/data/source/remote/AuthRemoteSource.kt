package com.appetiser.mycodingchallenge.data.source.remote

import com.appetiser.mycodingchallenge.data.AuthSource
import com.appetiser.mycodingchallenge.data.mapper.UserSessionMapper
import com.appetiser.mycodingchallenge.data.poko.AccessToken
import com.appetiser.mycodingchallenge.data.poko.UserSession
import com.appetiser.mycodingchallenge.domain.BaseplateApiServices
import com.appetiser.mycodingchallenge.data.poko.CountryCode
import com.google.gson.Gson
import com.google.gson.JsonObject
import io.reactivex.Completable
import io.reactivex.Single
import okhttp3.MediaType
import okhttp3.RequestBody
import sampleData.SampleData
import javax.inject.Inject

class AuthRemoteSource @Inject
constructor(private val baseplateApiServices: BaseplateApiServices, private val mapper: UserSessionMapper, private val gson: Gson) : AuthSource {

    override fun logout(): Completable {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun forgotPassword(email: String): Single<Boolean> {
        val userMap = hashMapOf<String, String>()
        userMap["email"] = email
        return baseplateApiServices.sendResetPassword(userMap)
                .map { it.isResponseSuccess }
    }

    override fun checkEmail(email: String): Single<Boolean> {
        val userMap = hashMapOf<String, String>()
        userMap["email"] = email

        return baseplateApiServices.checkEmailIfExists(userMap)
                .map { it.data.isEmailExists }
    }

    override fun login(email: String, password: String): Single<Map<String, Any>> {
        val userMap = hashMapOf<String, String>()
        userMap["email"] = email
        userMap["password"] = password
        return baseplateApiServices.loginUser(userMap)
                .map {
                    mapper.mapFromDomain(it)
                }
    }

    override fun saveCredentials(user: UserSession): Single<UserSession> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun saveToken(token: String): Single<AccessToken> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getUserSession(): Single<UserSession> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun register(email: String, password: String, confirmPassword: String, country: String, dob: String, mobileNumber: String,
                          firstName: String, lastName: String, address: String): Single<Map<String, Any>> {
        val map = hashMapOf<String, String>()
        map["email"] = email

        if (password.isNotEmpty()) {
            map["password"] = password
        }

        if (confirmPassword.isNotEmpty()) {
            map["password_confirmation"] = confirmPassword
        }

        if (firstName.isNotEmpty()) {
            map["first_name"] = firstName
        }

        if (lastName.isNotEmpty()) {
            map["last_name"] = lastName
        }

        if (mobileNumber.isNotEmpty()) {
            map["mobile_number"] = mobileNumber
        }

        if (country.isNotEmpty()) {
            map["country"] = country
        }

        if (dob.isNotEmpty()) {
            map["dob"] = dob
        }

        if (address.isNotEmpty()) {
            map["address"] = address
        }

        return baseplateApiServices.registerUser(map)
                .map {
                    mapper.mapFromDomain(it)
                }
    }

    override fun verifyAccount(uid: String, code: String): Single<Boolean> {
        val map = hashMapOf<String, String>()
        map["uid"] = uid
        map["verification_code"] = code
        return baseplateApiServices.verifyUser(map)
                .map { it.isResponseSuccess }
    }

    override fun resendVerificationCode(email: String): Single<Boolean> {
        val map = hashMapOf<String, String>()
        map["email"] = email
        return baseplateApiServices.resendVerificationCode(map)
                .map {
                    it.isResponseSuccess
                }
    }

    override fun getCountryCode(countryCodes: List<Int>): Single<List<CountryCode>> {
        val jsonObject = JsonObject()

        if (countryCodes.isNotEmpty()) {
            jsonObject.add("calling_codes", gson.toJsonTree(countryCodes.toList()).asJsonArray)
        }

        val countryCode = if (countryCodes.isNotEmpty())
            baseplateApiServices.getCountryCodes(RequestBody.create(MediaType.parse("application/json"), jsonObject.toString()))
        else
            baseplateApiServices.getAllCountryCodes()

        return countryCode
                .toObservable()
                .flatMapIterable {
                    it.data.countries
                }
                .map {
                    CountryCode(
                            id = it.id,
                            name = it.name,
                            callingCode = it.callingCode,
                            flag = it.flag.orEmpty()
                    )
                }
                .toList()
                .onErrorResumeNext(Single.just(SampleData.stubCountries()))
    }

}






