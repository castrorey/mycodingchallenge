package com.appetiser.mycodingchallenge.data.poko

import com.google.gson.annotations.SerializedName

data class Track (
        @field:SerializedName("trackId")
        val trackId: Double,
        @field:SerializedName( "trackName")
        val trackName: String = "",
        @field:SerializedName( "artworkUrl60")
        val artworkSmall:String = "",
        @field:SerializedName("artworkUrl100")
        val artworkBig:String = "",
        @field:SerializedName("trackPrice")
        val trackPrice: Float,
        @field:SerializedName( "primaryGenreName")
        val genre: String="",
        @field:SerializedName("longDescription")
        val longDescription: String?
)