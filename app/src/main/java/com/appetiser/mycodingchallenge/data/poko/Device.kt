package com.appetiser.mycodingchallenge.data.poko

data class Device(var width: Int, var height: Int)
