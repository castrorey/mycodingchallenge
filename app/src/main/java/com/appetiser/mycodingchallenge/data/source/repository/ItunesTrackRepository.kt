package com.appetiser.mycodingchallenge.data.source.repository

import com.appetiser.mycodingchallenge.data.poko.Track
import com.appetiser.mycodingchallenge.data.ItunesTrackSource
import com.appetiser.mycodingchallenge.data.source.local.ItunesTrackLocalSource
import com.appetiser.mycodingchallenge.data.source.remote.ItunesTrackRemoteSource
import com.appetiser.mycodingchallenge.utils.Constants
import io.reactivex.Single
import javax.inject.Inject

class ItunesTrackRepository @Inject constructor(
        private val itunesTrackLocalSource: ItunesTrackLocalSource,
        private val itunesTrackRemoteSource: ItunesTrackRemoteSource): ItunesTrackSource, ItunesTrackSource.Local {

    override fun saveCurrentScreenState(lastVisitedScreen: Constants.Companion.SCREEN) = itunesTrackLocalSource.saveCurrentScreenState(lastVisitedScreen)

    override fun getCurrentScreenState(): Constants.Companion.SCREEN = itunesTrackLocalSource.getCurrentScreenState()

    override fun saveTrackList(trackList: List<Track>) = itunesTrackLocalSource.saveTrackList(trackList)

    override fun saveTrackListRefreshedDate()  = itunesTrackLocalSource.saveTrackListRefreshedDate()

    override fun getTrackListRefreshedDate(): String = itunesTrackLocalSource.getTrackListRefreshedDate()

    override fun getTrackList(isRefreshed: Boolean): Single<List<Track>>{
        return when(isRefreshed){
            true -> {
                itunesTrackRemoteSource.getTrackList(true)}
            false ->
                itunesTrackLocalSource.getTrackList(false)
        }
    }

    override fun saveTrack(track: Track) {
        itunesTrackLocalSource.saveTrack(track)
    }

    override fun getSavedTrack(): Track = itunesTrackLocalSource.getSavedTrack()
}