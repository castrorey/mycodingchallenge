package com.appetiser.mycodingchallenge.data.source.remote

import com.appetiser.mycodingchallenge.api.ApiServices
import com.appetiser.mycodingchallenge.data.poko.Track
import com.appetiser.mycodingchallenge.data.ItunesTrackSource
import io.reactivex.Single
import javax.inject.Inject

class ItunesTrackRemoteSource @Inject constructor(private val apiServices: ApiServices) : ItunesTrackSource {

    override fun getTrackList(isRefreshed: Boolean): Single<List<Track>> {
        val track = apiServices.fetchAllSearchList()
        return track.map { it.results }
    }
}