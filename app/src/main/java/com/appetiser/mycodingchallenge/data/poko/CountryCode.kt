package com.appetiser.mycodingchallenge.data.poko

import com.appetiser.mycodingchallenge.di.NetworkModule

class CountryCode(
        val id: Long = 0,
        val name: String = "",
        val callingCode: String = "",
        val flag: String = ""
) {

    val flagUrl = "${NetworkModule.BASE_URL}$flag"
}
