package com.appetiser.mycodingchallenge.data.source.local

import android.content.SharedPreferences
import com.appetiser.android.baseplate.persistence.AppDatabase
import com.appetiser.android.baseplate.persistence.model.DBToken
import com.appetiser.android.baseplate.persistence.model.DBUserSession
import com.appetiser.mycodingchallenge.data.AuthSource
import com.appetiser.mycodingchallenge.data.mapper.UserSessionMapper
import com.appetiser.mycodingchallenge.data.poko.AccessToken
import com.appetiser.mycodingchallenge.data.poko.UserSession
import com.appetiser.mycodingchallenge.data.poko.CountryCode
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class AuthLocalSource @Inject
constructor(private val database: AppDatabase,
            private val sharedPreferences: SharedPreferences,
            private val mapper: UserSessionMapper) : AuthSource {

    override fun forgotPassword(email: String): Single<Boolean> {
        return Single.just(false)
    }

    override fun logout(): Completable {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun checkEmail(email: String): Single<Boolean> = Single.just(false)

    override fun login(email: String, password: String): Single<Map<String, Any>> = Single.just(mapOf())

    override fun saveCredentials(user: UserSession): Single<UserSession> {
        return Single.create { emitter ->
            val dbUser = mapper.mapFromData(user)
            database.userSessionDao().insert(dbUser)
            emitter.onSuccess(user)
        }
    }

    override fun saveToken(token: String): Single<AccessToken> {
        return Single.create { emitter ->
            database.tokenDao().insert(DBToken(token = token))
            emitter.onSuccess(AccessToken(token))
        }
    }

    override fun getUserSession(): Single<UserSession> {
        return database.userSessionDao().getUserInfo()
                .map {
                    mapper.mapFromDB(it)
                }
    }

    override fun register(email: String, password: String, confirmPassword: String, country: String, dob: String, mobileNumber:
    String, firstName: String, lastName: String, address: String): Single<Map<String, Any>> = Single.just(mapOf())

    override fun verifyAccount(uid: String, code: String): Single<Boolean> = Single.just(false)

    override fun resendVerificationCode(email: String): Single<Boolean> {
        return Single.just(false)
    }

    override fun getCountryCode(countryCodes: List<Int>): Single<List<CountryCode>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
