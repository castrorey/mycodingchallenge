package com.appetiser.mycodingchallenge.data.source.local

import android.content.SharedPreferences
import com.appetiser.mycodingchallenge.data.poko.Track
import com.appetiser.mycodingchallenge.data.ItunesTrackSource
import com.appetiser.mycodingchallenge.utils.Constants
import com.appetiser.mycodingchallenge.utils.Constants.Companion.PREF_KEY_PREVIOUSLY_VISITED_DATE
import com.appetiser.mycodingchallenge.utils.Constants.Companion.PREF_KEY_SAVED_TRACK_LIST
import com.appetiser.mycodingchallenge.utils.PreferencesHelper.get
import com.appetiser.mycodingchallenge.utils.PreferencesHelper.set
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.Single
import io.reactivex.rxkotlin.toObservable
import sampleData.SampleData
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class ItunesTrackLocalSource @Inject constructor(
        private val sharedPreferences: SharedPreferences,
        private val dateFormat: SimpleDateFormat,
        private val gson: Gson): ItunesTrackSource, ItunesTrackSource.Local {

    override fun saveTrackList(trackList: List<Track>) {
        sharedPreferences[PREF_KEY_SAVED_TRACK_LIST] = gson.toJson(trackList)
    }

    override fun getTrackListRefreshedDate(): String =
        sharedPreferences[PREF_KEY_PREVIOUSLY_VISITED_DATE]

    override fun saveTrackListRefreshedDate() {
        sharedPreferences[PREF_KEY_PREVIOUSLY_VISITED_DATE] = dateFormat.format(Date())
    }


    override fun getTrackList(isRefreshed: Boolean): Single<List<Track>> {
        val type = object : TypeToken<List<Track>>() {
        }.type
        val trackListString: String? = sharedPreferences[PREF_KEY_SAVED_TRACK_LIST]
        val trackList: List<Track> =  gson.fromJson(trackListString, type)
        return trackList
                .toObservable()
                .map {
                    Track(
                            trackId = it.trackId,
                            trackName = it.trackName,
                            trackPrice = it.trackPrice,
                            artworkBig = it.artworkBig,
                            artworkSmall = it.artworkSmall,
                            longDescription = it.longDescription
                    )
                }
                .toList()
                .onErrorResumeNext(Single.just(SampleData.stubTracks()))
    }

    override fun saveCurrentScreenState(lastVisitedScreen: Constants.Companion.SCREEN) {
        sharedPreferences[Constants.PREF_KEY_SCREEN] = lastVisitedScreen.screentype
    }

    override fun getCurrentScreenState(): Constants.Companion.SCREEN {
        return when (sharedPreferences[Constants.PREF_KEY_SCREEN, 0]) {
            Constants.Companion.SCREEN.LIST_FRAGMENT.screentype ->
                Constants.Companion.SCREEN.LIST_FRAGMENT
            Constants.Companion.SCREEN.DETAIL_FRAGMENT.screentype ->
                Constants.Companion.SCREEN.DETAIL_FRAGMENT
            else -> Constants.Companion.SCREEN.LIST_FRAGMENT
        }

    }

    override fun saveTrack(track: Track) {
        sharedPreferences[Constants.PREF_KEY_SAVED_TRACK] = gson.toJson(track)
    }

    override fun getSavedTrack(): Track {
        val trackString: String = sharedPreferences[Constants.PREF_KEY_SAVED_TRACK]
        return gson.fromJson(trackString, Track::class.java)
    }
}