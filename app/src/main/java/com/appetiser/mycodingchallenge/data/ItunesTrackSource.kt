package com.appetiser.mycodingchallenge.data

import com.appetiser.mycodingchallenge.data.poko.Track
import com.appetiser.mycodingchallenge.utils.Constants
import io.reactivex.Single

interface ItunesTrackSource {

    fun getTrackList(isRefreshed: Boolean): Single<List<Track>>

    interface Local {
    // For Local Source
        fun saveTrackList(trackList: List<Track>)

        fun saveTrackListRefreshedDate()

        fun getTrackListRefreshedDate(): String

        fun saveCurrentScreenState(lastVisitedScreen: Constants.Companion.SCREEN)

        fun getCurrentScreenState():Constants.Companion.SCREEN

        fun saveTrack(track: Track)

        fun getSavedTrack(): Track
    }
}