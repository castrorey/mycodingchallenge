package com.appetiser.mycodingchallenge.features.auth

import android.content.SharedPreferences
import com.appetiser.android.baseplate.persistence.AppDatabase
import com.appetiser.mycodingchallenge.data.mapper.UserSessionMapper
import com.appetiser.mycodingchallenge.data.source.local.AuthLocalSource
import com.appetiser.mycodingchallenge.data.source.remote.AuthRemoteSource
import com.appetiser.mycodingchallenge.data.source.repository.AuthRepository
import com.appetiser.mycodingchallenge.di.scopes.ActivityScope
import com.appetiser.mycodingchallenge.domain.BaseplateApiServices
import com.google.gson.Gson
import dagger.Module
import dagger.Provides

@Module
class AuthRepositoryModule {

    @ActivityScope
    @Provides
    fun providesAuthLocalSource(database: AppDatabase, sharedPreferences: SharedPreferences, mapper: UserSessionMapper):
            AuthLocalSource = AuthLocalSource(database, sharedPreferences, mapper)

    @ActivityScope
    @Provides
    fun providesAuthRemoteSource(baseplateApiServices: BaseplateApiServices, mapper: UserSessionMapper, gson: Gson):
            AuthRemoteSource = AuthRemoteSource(baseplateApiServices, mapper, gson)

    @ActivityScope
    @Provides
    fun providesAuthRepository(remote: AuthRemoteSource, local: AuthLocalSource): AuthRepository =
            AuthRepository(remote, local)

}
