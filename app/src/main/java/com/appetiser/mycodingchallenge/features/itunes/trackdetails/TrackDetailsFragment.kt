package com.appetiser.mycodingchallenge.features.itunes.trackdetails

import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import androidx.lifecycle.ViewModelProviders
import com.appetiser.mycodingchallenge.R
import com.appetiser.mycodingchallenge.ViewModelFactory
import com.appetiser.mycodingchallenge.base.BaseFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import javax.inject.Inject

/**
 * A simple [Fragment] that displays the selected track from the track list.
 *
 * Use the [TrackDetailsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TrackDetailsFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelFactory

    private lateinit var viewModel: TrackDetailsFragmentViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment and Initialize views
        val contentView =  inflater.inflate(R.layout.fragment_track_details, container, false)
        val imageViewArtWork = contentView.findViewById(R.id.imageView2) as ImageView
        val textViewTrackName = contentView.findViewById(R.id.textViewTrackName) as TextView
        val textViewDescription = contentView.findViewById<TextView>(R.id.textViewDescription)
        viewModel = ViewModelProviders.of(this, factory).get(TrackDetailsFragmentViewModel::class.java)
        if (savedInstanceState == null) {
            viewModel.saveCurrentScreen()
            viewModel.getSavedTrack()?.let { track ->
                Glide.with(contentView).load(track.artworkBig).apply(RequestOptions.fitCenterTransform())
                    .placeholder(R.drawable.ic_image_loading).into(imageViewArtWork)
                track.longDescription?.let { description -> textViewDescription.text = description }
                    ?: kotlin.run { textViewDescription.text = getString(R.string.no_description) }
                textViewTrackName.text = track.trackName
            }
        }
        return contentView
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment TrackDetailsFragment.
         */
        @JvmStatic
        fun newInstance() =
                TrackDetailsFragment()
            }
    }
