package com.appetiser.mycodingchallenge.features.auth.register.verify

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.appetiser.mycodingchallenge.data.source.repository.AuthRepository
import com.appetiser.mycodingchallenge.utils.schedulers.BaseSchedulerProvider
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject


class VerifyAccountViewModel @Inject constructor(
        private val repository: AuthRepository,
        private val schedulers: BaseSchedulerProvider,
        app: Application)
    : AndroidViewModel(app) {


    private val _state: MutableLiveData<VerifyAccountState> by lazy {
        MutableLiveData<VerifyAccountState>()
    }

    val state: LiveData<VerifyAccountState>
        get() {
            return _state
        }


    fun verifyRegistration(verificationCode: String) : Disposable {
        _state.value = VerifyAccountState.ShowProgressLoading
        return repository.getUserSession()
                .flatMap {
                    repository.verifyAccount(it.uid, verificationCode)
                }
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribeBy(onSuccess = {
                    _state.value = VerifyAccountState.HideProgressLoading
                    if(it)
                        _state.value = VerifyAccountState.Success(it)
                    else
                        _state.value = VerifyAccountState.Error(Throwable("Something went wrong"))
                },onError = {
                    _state.value = VerifyAccountState.HideProgressLoading
                    _state.value = VerifyAccountState.Error(it)
                })
    }

    fun resendVerificationCode() : Disposable {
        _state.value = VerifyAccountState.ShowProgressLoading
        return  repository.getUserSession()
                .flatMap {
                    repository.resendVerificationCode(it.email)
                }
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribeBy(
                        onSuccess = {
                            _state.value = VerifyAccountState.HideProgressLoading
                            if(it)
                                _state.value = VerifyAccountState.ResendVerification(it)
                            else
                                _state.value = VerifyAccountState.Error(Throwable("Something went wrong"))
                        },
                        onError = {
                            _state.value = VerifyAccountState.HideProgressLoading
                            _state.value = VerifyAccountState.Error(it)
                        }
                )

    }

}
