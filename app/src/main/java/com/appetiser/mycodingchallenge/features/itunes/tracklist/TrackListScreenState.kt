package com.appetiser.mycodingchallenge.features.itunes.tracklist

import com.appetiser.mycodingchallenge.data.poko.Track


sealed class TrackListScreenState {
    data class Loading(val isLoading: Boolean): TrackListScreenState()
    data class Success(val trackList: List<Track>, val dateRefreshed: String): TrackListScreenState()
    data class Error(val errorMessage: String): TrackListScreenState()
}