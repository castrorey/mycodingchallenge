package com.appetiser.mycodingchallenge.features.auth.register

import com.appetiser.mycodingchallenge.data.poko.CountryCode
import com.appetiser.mycodingchallenge.data.poko.UserSession

sealed class RegisterState {
    data class SaveLoginCredentials(val user: UserSession) : RegisterState()
    data class GetCountryCode(val countryCode: List<CountryCode>) : RegisterState()
    data class Error(val throwable: Throwable): RegisterState()
    object ShowProgressLoading : RegisterState()
    object HideProgressLoading : RegisterState()
}
