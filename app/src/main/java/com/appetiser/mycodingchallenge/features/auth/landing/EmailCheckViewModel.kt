package com.appetiser.mycodingchallenge.features.auth.landing

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.appetiser.mycodingchallenge.data.source.repository.AuthRepository
import com.appetiser.mycodingchallenge.features.auth.landing.EmailState
import com.appetiser.mycodingchallenge.utils.schedulers.BaseSchedulerProvider
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class EmailCheckViewModel @Inject constructor(
        private val repository: AuthRepository,
        private val schedulers: BaseSchedulerProvider,
        app: Application)
    : AndroidViewModel(app) {


    private val _state: MutableLiveData<EmailState> by lazy {
        MutableLiveData<EmailState>()
    }

    val state: LiveData<EmailState>
        get() {
            return _state
        }

    fun checkEmail(email: String): Disposable {
        _state.value = EmailState.ShowProgressLoading
        return repository.checkEmail(email)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {
                    _state.value = EmailState.HideProgressLoading
                }
                .subscribe({
                    if (it) {
                        _state.value = EmailState.EmailExists(email)
                    } else {
                        _state.value = EmailState.EmailDoesNotExist(email)
                    }
                }, {
                    _state.value = EmailState.Error(it)
                })
    }
}
