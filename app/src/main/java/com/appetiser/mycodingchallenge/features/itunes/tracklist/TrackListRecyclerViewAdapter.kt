package com.castrorr.itunessearchlist.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.appetiser.mycodingchallenge.R
import com.appetiser.mycodingchallenge.data.poko.Track
import com.appetiser.mycodingchallenge.ext.loadImageUrl
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_track_list.view.*


/**
 * This class is an adapter for the recycler view
 *  @param itemClick which gets the track selected by the user to be saved in sharedPreferences
 *
 *  */

class TrackListRecyclerViewAdapter( private val itemClick: (Track) -> Unit):
    ListAdapter<Track, TrackListRecyclerViewAdapter.ViewHolder> ((PostDiffCallback())) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(private val parent: ViewGroup) :
        RecyclerView.ViewHolder(
            LayoutInflater.from(parent.context).inflate(
              R.layout.item_track_list,
                parent,false
            )
        ) {

        fun bind(item: Track) {
            itemView.imageViewArtwork.loadImageUrl(item.artworkBig)
            itemView.textViewTrackName.text = item.trackName
            itemView.textViewGenre.text = item.genre
            itemView.textViewPrice.text = """${"$"}${item.trackPrice}"""
            itemView.setOnClickListener { itemClick.invoke(item) }
        }

    }
}

/**
 * This class and Callback when the user selects an item in the list
 */
    private class PostDiffCallback : DiffUtil.ItemCallback<Track>() {
        override fun areItemsTheSame(oldItem: Track, newItem: Track): Boolean =
            oldItem.trackId == newItem.trackId

        override fun areContentsTheSame(oldItem: Track, newItem: Track): Boolean =
            oldItem == newItem
    }
