package com.appetiser.mycodingchallenge.features.auth.forgotpassword

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.appetiser.mycodingchallenge.R
import com.appetiser.mycodingchallenge.base.DaggerBaseActivity
import com.appetiser.mycodingchallenge.ext.enableWhen
import com.appetiser.mycodingchallenge.ext.toast
import com.appetiser.mycodingchallenge.domain.response.error.ResponseError
import sampleData.SampleData

class ForgotPasswordActivity : DaggerBaseActivity() {

    companion object {
        private const val EMAIL = "email"

        fun openActivity(context: Context) {
            context.startActivity(Intent(context, ForgotPasswordActivity::class.java))
        }
    }

    private lateinit var viewModel: ForgotPasswordViewModel

    private lateinit var btnContinue: AppCompatButton

    private lateinit var etEmail: AppCompatEditText

    private lateinit var toolbarView: Toolbar

    private lateinit var toolbarTitle: AppCompatTextView

    override fun getLayoutResources(): Int {
        return R.layout.activity_forgot_password
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpViews(savedInstanceState)
        setUpViewModels()
        setupToolbar()
        etEmail.setText(SampleData.USER_NAME)
    }


    /**
     * Found an issue using the kotlinx when access layouts from library
     *
     * https://stackoverflow.com/questions/34169562/unresolved-reference-kotlinx
     * https://stackoverflow.com/questions/48378696/unresolved-reference-for-synthetic-view-when-layout-is-in-library-module/54123767#54123767
     *
     * as a workaround will be using findViewById()
     */
    private fun setUpViews(savedInstanceState: Bundle?) {
        btnContinue = this.findViewById(R.id.btnContinue)
        etEmail = this.findViewById(R.id.etEmail)


        etEmail.apply {
            setText(savedInstanceState?.getString(ForgotPasswordActivity.EMAIL))
        }

        btnContinue.enableWhen(etEmail) { Patterns.EMAIL_ADDRESS.matcher(it).matches() }
        btnContinue.setOnClickListener {
            viewModel.forgotPassword(etEmail.text.toString())
        }

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(EMAIL, etEmail.text.toString())
    }

    private fun setUpViewModels() {
        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(ForgotPasswordViewModel::class.java)

        viewModel.state.observe(this, Observer {
            when (it) {
                is ForgotPasswordState.Success -> {
                    toast(getString(R.string.forgot_password_success_message, etEmail.text.toString()))
                }

                is ForgotPasswordState.Error -> {
                    ResponseError.getError(it.throwable,
                            ResponseError.ErrorCallback(httpExceptionCallback = {
                                toast("Error $it")
                            }))
                }

                is ForgotPasswordState.ShowProgressLoading -> {
                    toast("Sending request")
                }
            }
        })
    }

    private fun setupToolbar() {
        toolbarView = findViewById(R.id.toolbarView)
        toolbarTitle = toolbarView.findViewById(R.id.toolbarTitle)

        setSupportActionBar(toolbarView)

        supportActionBar?.apply {
            setDisplayShowTitleEnabled(false)
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
            setDisplayShowCustomEnabled(true)
        }

        toolbarView.setNavigationOnClickListener { onBackPressed() }
        toolbarTitle.text = getString(R.string.forgot_password)
    }

}
