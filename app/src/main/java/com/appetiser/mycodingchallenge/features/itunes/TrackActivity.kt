package com.appetiser.mycodingchallenge.features.itunes

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.appetiser.mycodingchallenge.R
import com.appetiser.mycodingchallenge.ViewModelFactory
import com.appetiser.mycodingchallenge.base.DaggerBaseActivity
import com.appetiser.mycodingchallenge.data.poko.Track
import com.appetiser.mycodingchallenge.di.Injectable
import com.appetiser.mycodingchallenge.features.auth.login.LoginActivity
import com.appetiser.mycodingchallenge.features.itunes.trackdetails.TrackDetailsFragment
import com.appetiser.mycodingchallenge.utils.Constants.Companion.SCREEN
import com.appetiser.mycodingchallenge.features.itunes.tracklist.TrackListFragment
import com.appetiser.mycodingchallenge.features.main.MainActivity
import kotlinx.android.synthetic.main.activity_track.*
import javax.inject.Inject

class TrackActivity: DaggerBaseActivity(), Injectable, TrackListFragment.OnFragmentInteractionListener {

    companion object {
        fun openActivity(context: Context, screenType: Int) {
            val intent = Intent(context, TrackActivity::class.java)
            intent.putExtra(KEY_SCREEN_TYPE, screenType)
            context.startActivity(intent)
        }
        private const val KEY_SCREEN_TYPE = "screen_type"
    }

    private lateinit var currentFragment: Fragment
    @Inject
    lateinit var factory: ViewModelFactory

    lateinit var viewModel: TrackActivityViewModel

    override fun getLayoutResources(): Int {
        return R.layout.activity_track
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpToolbar()
        viewModel = ViewModelProviders.of(this, factory).get(TrackActivityViewModel::class.java)
        restorePreviouslyVisitedScreen()
    }

    private fun restorePreviouslyVisitedScreen(){
        intent?.extras?.let {
            when (it.getInt(KEY_SCREEN_TYPE)) {
                SCREEN.DETAIL_FRAGMENT.screentype ->
                    showDetailFragment()
                SCREEN.LIST_FRAGMENT.screentype ->
                    showListFragment()
                else -> showListFragment()
            }
        }

    }

    override fun onAttachFragment(fragment: Fragment) {
        if (fragment is TrackListFragment) {
            viewModel.saveCurrentScreen(SCREEN.LIST_FRAGMENT)
            fragment.setOnFragmentInteractionListener(this)
        } else if(fragment is TrackDetailsFragment) {
            viewModel.saveCurrentScreen(SCREEN.DETAIL_FRAGMENT)
        }
        currentFragment = fragment

    }

    private fun showListFragment(){
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        val searchiTunesListFragment =
                TrackListFragment.newInstance()
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, searchiTunesListFragment)
                .addToBackStack(null)
                .commit()
    }

    override fun onBackPressed() {
        if (currentFragment is TrackListFragment)
            finish()
        else if(currentFragment is TrackDetailsFragment)
            showListFragment()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId){
            android.R.id.home ->
                showListFragment()
        }
        return super.onOptionsItemSelected(item)
    }



    private fun showDetailFragment() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val searchiTunesDetailFragment =
                TrackDetailsFragment.newInstance()
        supportFragmentManager
                .beginTransaction()
                .add(R.id.container, searchiTunesDetailFragment)
                .addToBackStack(null)
                .commit()
    }

    private fun setUpToolbar(){
       //setSupportActionBar(toolbar)
    }

    /**
     * This method is an implementation
     * from the SearchiTunesListFragment.OnFragmentInteractionListener
     */
    override fun onListItemClick(track: Track) {
        viewModel.saveTrackToPreference(track)
        showDetailFragment()
    }
}