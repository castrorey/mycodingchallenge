package com.appetiser.mycodingchallenge.features.itunes

import com.appetiser.mycodingchallenge.api.ApiServices
import com.appetiser.mycodingchallenge.data.source.local.ItunesTrackLocalSource
import com.appetiser.mycodingchallenge.data.source.remote.ItunesTrackRemoteSource
import com.appetiser.mycodingchallenge.data.source.repository.ItunesTrackRepository
import com.appetiser.mycodingchallenge.di.scopes.ActivityScope
import dagger.Module
import dagger.Provides

@Module
class TrackRepositoryModule {

    @Provides
    fun provideItunesTrackRemoteSource(apiServices: ApiServices):
            ItunesTrackRemoteSource = ItunesTrackRemoteSource(apiServices)

    @Provides
    fun providesItunesTrackRepository(itunesTrackLocalSource: ItunesTrackLocalSource,itunesTrackRemoteSource: ItunesTrackRemoteSource):
            ItunesTrackRepository = ItunesTrackRepository(itunesTrackLocalSource, itunesTrackRemoteSource )
}