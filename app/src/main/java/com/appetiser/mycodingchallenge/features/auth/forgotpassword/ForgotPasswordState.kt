package com.appetiser.mycodingchallenge.features.auth.forgotpassword


sealed class ForgotPasswordState {

    data class Success(val isSuccess: Boolean) : ForgotPasswordState()

    data class Error(val throwable: Throwable): ForgotPasswordState()

    object ShowProgressLoading : ForgotPasswordState()

    object HideProgressLoading : ForgotPasswordState()

}
