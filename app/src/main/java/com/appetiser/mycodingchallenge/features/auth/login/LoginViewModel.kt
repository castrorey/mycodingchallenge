package com.appetiser.mycodingchallenge.features.auth.login

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.appetiser.mycodingchallenge.data.mapper.getUserSession
import com.appetiser.mycodingchallenge.data.source.repository.AuthRepository
import com.appetiser.mycodingchallenge.utils.schedulers.BaseSchedulerProvider
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class LoginViewModel @Inject constructor(
        private val repository: AuthRepository,
        private val schedulers: BaseSchedulerProvider,
        app: Application)
    : AndroidViewModel(app) {


    private val _state: MutableLiveData<LoginState> by lazy {
        MutableLiveData<LoginState>()
    }

    val state: LiveData<LoginState>
        get() {
            return _state
        }

    fun login(email: String, password: String): Disposable {
        _state.value = LoginState.ShowProgressLoading
        return repository.login(email, password)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {
                    _state.value = LoginState.HideProgressLoading
                }
                .subscribe({
                    val user = it.getUserSession()

                    if (user.uid.isNotEmpty()) {
                        _state.value = LoginState.LoginSuccess(user)

                    }
                }, {
                    _state.value = LoginState.Error(it)
                })
    }

}
