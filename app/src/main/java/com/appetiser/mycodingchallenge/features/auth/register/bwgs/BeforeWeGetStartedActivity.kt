package com.appetiser.mycodingchallenge.features.auth.register.bwgs

import com.appetiser.mycodingchallenge.R
import com.appetiser.mycodingchallenge.base.DaggerBaseActivity


class BeforeWeGetStartedActivity : DaggerBaseActivity() {
    override fun getLayoutResources(): Int =
        R.layout.activity_onboard_register_step0
}
