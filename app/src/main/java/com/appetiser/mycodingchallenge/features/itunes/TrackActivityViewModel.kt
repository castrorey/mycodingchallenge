package com.appetiser.mycodingchallenge.features.itunes

import android.app.Application
import com.appetiser.mycodingchallenge.base.BaseViewModel
import com.appetiser.mycodingchallenge.data.poko.Track
import com.appetiser.mycodingchallenge.data.source.repository.ItunesTrackRepository
import com.appetiser.mycodingchallenge.utils.Constants
import javax.inject.Inject

class TrackActivityViewModel @Inject constructor(
        application: Application,
        private val trackRepository: ItunesTrackRepository):BaseViewModel(application) {


    /**
     * This method saves the selected track
     * in the sharedPreference which
     * it will be used when the app restores
     */
    fun saveTrackToPreference(track: Track){
       trackRepository.saveTrack(track)
    }


    fun saveCurrentScreen(screen: Constants.Companion.SCREEN){
        trackRepository.saveCurrentScreenState(screen)
    }

    fun getSavedScreen():Constants.Companion.SCREEN = trackRepository.getCurrentScreenState()
}