package com.appetiser.mycodingchallenge.features.itunes.trackdetails

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.appetiser.mycodingchallenge.base.BaseViewModel
import com.appetiser.mycodingchallenge.data.poko.Track
import com.appetiser.mycodingchallenge.data.source.repository.ItunesTrackRepository
import com.appetiser.mycodingchallenge.utils.Constants
import com.appetiser.mycodingchallenge.utils.PreferencesHelper
import com.google.gson.Gson
import sampleData.SampleData
import javax.inject.Inject

class TrackDetailsFragmentViewModel @Inject constructor(
        application: Application,
        private val repository: ItunesTrackRepository): BaseViewModel(application) {

    /**
     * returns the selected track from the sharedPreference
     */
    fun getSavedTrack(): Track? {
       return repository.getSavedTrack()
   }

    /**
     * Saves the current screen to the sharedPreference
     * The saved screen will be restored when the user opens the app
     */

    fun saveCurrentScreen(){
       repository.saveCurrentScreenState(Constants.Companion.SCREEN.DETAIL_FRAGMENT)
    }
}