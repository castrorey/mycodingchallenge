package com.appetiser.mycodingchallenge.features.itunes.tracklist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.isInvisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.appetiser.mycodingchallenge.R
import com.appetiser.mycodingchallenge.ViewModelFactory
import com.appetiser.mycodingchallenge.base.BaseFragment
import com.appetiser.mycodingchallenge.data.poko.Track
import com.castrorr.itunessearchlist.view.TrackListRecyclerViewAdapter
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [TrackListFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [TrackListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TrackListFragment : BaseFragment() {
    private var listener: OnFragmentInteractionListener? = null

    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var mAdapter: TrackListRecyclerViewAdapter
    private lateinit var textViewDate: TextView

    @Inject lateinit var factory: ViewModelFactory

    private val viewModel: TrackListFragmentViewModel by lazy {
        ViewModelProviders.of(this,factory).get(TrackListFragmentViewModel::class.java)
    }

    private val itemClick: (Track) -> Unit =
        {
            listener?.onListItemClick(track = it)
        }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val contentView = inflater.inflate(R.layout.fragment_track_list, container, false)
        val recyclerView  = contentView.findViewById<RecyclerView>(R.id.search_list_recyclerView)
        swipeRefreshLayout = contentView.findViewById(R.id.swipe_refresh) as SwipeRefreshLayout
        textViewDate = contentView.findViewById(R.id.textViewDate)
        mAdapter = TrackListRecyclerViewAdapter(itemClick)
        recyclerView.apply {
            layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
            adapter = mAdapter
            setHasFixedSize(true)
        }

        if (savedInstanceState == null) {
            viewModel.loadList(true)
        }
        viewModel.state.observe(this, Observer { updateLists(it) })
        swipeRefreshLayout.setOnRefreshListener { viewModel.loadList(true) }
        return contentView
        }

    /**
     * This method updates the track list when the user refreshes the view
     * and submit the list to the adapter.
     * It also sets the visibility of the textView that displays the date when the user previously visited
     */
    private fun updateLists(trackListScreenState: TrackListScreenState?) {
        trackListScreenState?.let {
            when (it) {
               is TrackListScreenState.Success -> {
                   mAdapter.submitList(it.trackList)
                   textViewDate.visibility = View.VISIBLE
                   textViewDate.text = it.dateRefreshed
                   swipeRefreshLayout.isRefreshing = false
               }
                is TrackListScreenState.Error -> {
                    showSnackBarError()
                    textViewDate.visibility = View.INVISIBLE
                    swipeRefreshLayout.isRefreshing = false
                    mAdapter.submitList(null)
                }
                is TrackListScreenState.Loading -> {
                    swipeRefreshLayout.isRefreshing = true
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home ->
                activity?.finish()
        }
        return true
    }


    private fun showSnackBarError(){
        Snackbar.make(swipeRefreshLayout, getString(R.string.error), Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(R.string.retry)) { viewModel.loadList(true)
        }.show()
    }

    /**
     * This interface can be implemented by the Activity, parent Fragment,
     *   or a separate test implementation.
     */

    fun setOnFragmentInteractionListener(listener: OnFragmentInteractionListener){
        this.listener = listener
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        fun onListItemClick(track: Track)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment TrackListFragment.
         */
        @JvmStatic
        fun newInstance() =
            TrackListFragment()
    }
}
