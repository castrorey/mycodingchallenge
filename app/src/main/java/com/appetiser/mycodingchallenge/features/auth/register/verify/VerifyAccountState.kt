package com.appetiser.mycodingchallenge.features.auth.register.verify

sealed class VerifyAccountState {

    data class Success(val isSuccess: Boolean) : VerifyAccountState()

    data class Error(val throwable: Throwable): VerifyAccountState()

    data class ResendVerification(val isSuccess: Boolean): VerifyAccountState()

    object ShowProgressLoading : VerifyAccountState()

    object HideProgressLoading : VerifyAccountState()

}
