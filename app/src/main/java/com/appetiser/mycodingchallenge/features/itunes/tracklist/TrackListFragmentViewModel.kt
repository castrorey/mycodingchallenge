package com.appetiser.mycodingchallenge.features.itunes.tracklist

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.appetiser.mycodingchallenge.base.BaseViewModel
import com.appetiser.mycodingchallenge.data.source.repository.ItunesTrackRepository
import com.appetiser.mycodingchallenge.utils.schedulers.BaseSchedulerProvider
import io.reactivex.disposables.Disposable
import io.reactivex.disposables.Disposables
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject


class TrackListFragmentViewModel @Inject constructor(
        application: Application,
        private val schedulerProvider: BaseSchedulerProvider,
        val repository: ItunesTrackRepository): BaseViewModel(application) {


    private val _state: MutableLiveData<TrackListScreenState> by lazy {
        MutableLiveData<TrackListScreenState>()
    }

    val state: LiveData<TrackListScreenState>
        get() {
            return _state
        }

    //private val sharedPreferences = PreferencesHelper.customPrefs(getApplication(), Constants.PREF_KEY)

    /**
     * This method load the list when the user refreshes or opens the app.
     *
     */
    fun loadList(isRefreshed: Boolean)  {
        _state.value = TrackListScreenState.Loading(true)
          disposable.add(repository.getTrackList(isRefreshed)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(
                    {
                        repository.saveTrackListRefreshedDate()
                        _state.value = TrackListScreenState.Success(it, repository.getTrackListRefreshedDate())

                    }
                    ,{
                    _state.value = TrackListScreenState.Error(it.message!!)

                     }
                )
          )
    }

}