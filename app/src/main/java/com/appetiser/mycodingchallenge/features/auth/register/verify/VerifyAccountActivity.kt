package com.appetiser.mycodingchallenge.features.auth.register.verify

import android.os.Bundle
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.appetiser.mycodingchallenge.R
import com.appetiser.mycodingchallenge.base.DaggerBaseActivity
import com.appetiser.mycodingchallenge.domain.response.error.ResponseError
import com.appetiser.mycodingchallenge.ext.toast
import com.appetiser.verifycodeview.PinEntryEditText

class VerifyAccountActivity : DaggerBaseActivity() {

    lateinit var verifyAccountViewModel: VerifyAccountViewModel

    override fun getLayoutResources(): Int = R.layout.activity_onboard_register_step2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpViewModel()
        setUpViews()
    }

    fun setUpViewModel() {
        verifyAccountViewModel = ViewModelProviders.of(this, viewModelFactory).get(VerifyAccountViewModel::class.java)

        verifyAccountViewModel.state.observe(this, Observer {
            when (it) {

                is VerifyAccountState.Success -> {
                    // do something when success verifying code
                }

                is VerifyAccountState.Error -> {
                    ResponseError.getError(it.throwable,
                            ResponseError.ErrorCallback(httpExceptionCallback = {
                                toast("$it")
                            }))
                }

                is VerifyAccountState.ResendVerification -> {
                    // do something when success or error resending verification
                }

                is VerifyAccountState.ShowProgressLoading -> {
                    // show progress bar
                }

                is VerifyAccountState.HideProgressLoading -> {
                    // Hide progress bar
                }
            }
        })

    }

    private fun setUpViews() {

        val tvPinCode = findViewById<PinEntryEditText>(R.id.tvPinCode)
        val btnContinue = findViewById<AppCompatButton>(R.id.btnContinue)
        val codeHasntReceived = findViewById<AppCompatTextView>(R.id.codeHasntReceived)

        btnContinue.setOnClickListener { verifyAccountViewModel.verifyRegistration(tvPinCode.text.toString()) }

        codeHasntReceived.setOnClickListener { verifyAccountViewModel.resendVerificationCode() }
    }

}
