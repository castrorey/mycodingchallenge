package com.appetiser.mycodingchallenge.features.auth.register.accountdetail

import com.appetiser.mycodingchallenge.R
import com.appetiser.mycodingchallenge.base.DaggerBaseActivity

class AccountDetailsActivity : DaggerBaseActivity() {

    override fun getLayoutResources(): Int = R.layout.activity_onboard_register_step3

}
