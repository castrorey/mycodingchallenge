package com.appetiser.mycodingchallenge.api

import com.appetiser.mycodingchallenge.data.poko.Track
import com.appetiser.mycodingchallenge.domain.response.BaseResponse
import com.google.gson.annotations.SerializedName

data class APITrackResponse (
        @field:SerializedName ( "results")
        val results : List<Track>,
        @field:SerializedName( "resultCount")
        val resultCount: Int): BaseResponse()