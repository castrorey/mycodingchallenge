package com.appetiser.mycodingchallenge.api

import io.reactivex.Single
import retrofit2.http.GET

interface ApiServices  {
    @GET("search?term=star&amp;country=au&amp;media=movie&amp;all")
    fun fetchAllSearchList(): Single<APITrackResponse>
}
