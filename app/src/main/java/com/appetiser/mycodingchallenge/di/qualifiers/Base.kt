package com.appetiser.mycodingchallenge.di.qualifiers

import java.lang.annotation.Documented
import javax.inject.Qualifier

@Qualifier
@Documented
@Retention(AnnotationRetention.RUNTIME)
annotation class Named(
        /** The name.  */
        val value: String = "")