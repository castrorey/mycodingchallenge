package com.appetiser.mycodingchallenge.di

import com.appetiser.mycodingchallenge.api.ApiServices
import com.appetiser.mycodingchallenge.domain.BaseplateApiServices
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
class NetworkModule {

    companion object {
        const val BASE_URL = "http://128.199.193.26"
        const val BASE_URL2 = "https://itunes.apple.com/"
        const val API = "api"
        const val VERSION = "v1"
        const val ENDPOINT_FORMAT = "%s/%s/%s/"
    }

    @Provides
    @Singleton
    fun providesOkHttpClient(): OkHttpClient {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder()
                .addInterceptor(logging)
                .build()
    }

    @Provides
    @Singleton
    fun providesGson(): Gson {
        return GsonBuilder()
                .setLenient()
                .create()
    }

    @Provides
    @Singleton
    @Named("BasePlate")
    fun providesRetrofit(okHttpClient: OkHttpClient, gson: Gson): Retrofit {
        return Retrofit.Builder()
                .baseUrl(String.format(ENDPOINT_FORMAT, BASE_URL, API, VERSION))
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build()
    }

    @Provides
    @Singleton
    fun providesBaseplateApiServices(@Named("BasePlate")retrofit: Retrofit): BaseplateApiServices =
            retrofit.create(BaseplateApiServices::class.java)

    @Provides
    @Singleton
    @Named("Itunes")
    fun providesRetrofitItunes(okHttpClient: OkHttpClient, gson: Gson): Retrofit {
        return Retrofit.Builder()
                .baseUrl(BASE_URL2)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build()
    }

    @Provides
    @Singleton
    fun providesItunesApiService(@Named("Itunes")retrofit2: Retrofit): ApiServices =
            retrofit2.create(ApiServices::class.java)

}
