package com.appetiser.mycodingchallenge.di

import android.content.Context
import com.appetiser.mycodingchallenge.MycodingchallengeApplication
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class AppModule {

    @Singleton
    @Binds
    abstract fun providesApplicationContext(app: MycodingchallengeApplication) : Context
}
