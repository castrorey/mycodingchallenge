package com.appetiser.mycodingchallenge.di

import com.appetiser.mycodingchallenge.data.mapper.UserSessionMapper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MapperModule {

    @Provides
    @Singleton
    fun providesUserSessionMapper(): UserSessionMapper {
        return UserSessionMapper.getInstance()
    }

}
