package com.appetiser.mycodingchallenge.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
