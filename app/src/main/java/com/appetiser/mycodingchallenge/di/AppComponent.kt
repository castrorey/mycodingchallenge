package com.appetiser.mycodingchallenge.di

import android.app.Application
import com.appetiser.mycodingchallenge.MycodingchallengeApplication
import com.appetiser.mycodingchallenge.di.builders.ActivityBuilder
import com.appetiser.mycodingchallenge.di.builders.FragmentBuilder
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            (AndroidSupportInjectionModule::class),
            (MapperModule::class),
            (StorageModule::class),
            (NetworkModule::class),
            (ViewModelModernModule::class),
            (ActivityBuilder::class),
            (FragmentBuilder::class),
            (SchedulerModule::class),
            (ViewModelFactoryModule::class)
        ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: MycodingchallengeApplication)
}
