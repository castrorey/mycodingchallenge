package com.appetiser.mycodingchallenge.di

import androidx.lifecycle.ViewModel
import com.appetiser.mycodingchallenge.di.scopes.ViewModelKey
import com.appetiser.mycodingchallenge.features.auth.landing.EmailCheckViewModel
import com.appetiser.mycodingchallenge.features.auth.forgotpassword.ForgotPasswordViewModel
import com.appetiser.mycodingchallenge.features.auth.login.LoginViewModel
import com.appetiser.mycodingchallenge.features.auth.register.RegisterViewModel
import com.appetiser.mycodingchallenge.features.auth.register.verify.VerifyAccountViewModel
import com.appetiser.mycodingchallenge.features.itunes.TrackActivityViewModel
import com.appetiser.mycodingchallenge.features.itunes.trackdetails.TrackDetailsFragmentViewModel
import com.appetiser.mycodingchallenge.features.itunes.tracklist.TrackListFragmentViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModernModule {

    @Binds
    @IntoMap
    @ViewModelKey(EmailCheckViewModel::class)
    abstract fun bindEmailCheckViewModel(viewModel: EmailCheckViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ForgotPasswordViewModel::class)
    abstract fun bindForgotPasswordViewModel(viewModel: ForgotPasswordViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VerifyAccountViewModel::class)
    abstract fun bindVerifyAccountViewModel(viewModel: VerifyAccountViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RegisterViewModel::class)
    abstract fun bindVRegisterViewModel(viewModel: RegisterViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(TrackActivityViewModel::class)
    abstract fun bindTrackActivityViewModel(viewModel: TrackActivityViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TrackListFragmentViewModel::class)
    abstract fun bindTrackListFragmentViewModel(viewModel: TrackListFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TrackDetailsFragmentViewModel::class)
    abstract fun bindTrackDetailsFragmentViewModel(viewModel: TrackDetailsFragmentViewModel): ViewModel
}
