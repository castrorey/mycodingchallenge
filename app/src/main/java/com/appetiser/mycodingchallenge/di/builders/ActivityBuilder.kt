package com.appetiser.mycodingchallenge.di.builders

import com.appetiser.mycodingchallenge.di.scopes.ActivityScope
import com.appetiser.mycodingchallenge.features.auth.AuthRepositoryModule
import com.appetiser.mycodingchallenge.features.auth.emailcheck.EmailCheckActivity
import com.appetiser.mycodingchallenge.features.auth.forgotpassword.ForgotPasswordActivity
import com.appetiser.mycodingchallenge.features.auth.login.LoginActivity
import com.appetiser.mycodingchallenge.features.auth.register.RegisterActivity
import com.appetiser.mycodingchallenge.features.auth.register.verify.VerifyAccountActivity
import com.appetiser.mycodingchallenge.features.itunes.TrackActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeEmailCheckActivity(): EmailCheckActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeLoginActivity(): LoginActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeForgotPasswordActivity(): ForgotPasswordActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeVerifyAccountActivity(): VerifyAccountActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeRegisterActivity(): RegisterActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeTrackActivity() : TrackActivity

}
