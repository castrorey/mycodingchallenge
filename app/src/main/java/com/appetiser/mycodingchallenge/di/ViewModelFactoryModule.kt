package com.appetiser.mycodingchallenge.di

import androidx.lifecycle.ViewModelProvider
import com.appetiser.mycodingchallenge.ViewModelFactory
import dagger.Binds
import dagger.Module


@Suppress("unused")
@Module
abstract class ViewModelFactoryModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
