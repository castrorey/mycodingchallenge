package com.appetiser.mycodingchallenge.di.scopes


import javax.inject.Qualifier

@Qualifier
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
annotation class Local
