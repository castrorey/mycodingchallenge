package com.appetiser.mycodingchallenge.di

import com.appetiser.mycodingchallenge.utils.schedulers.BaseSchedulerProvider
import com.appetiser.mycodingchallenge.utils.schedulers.SchedulerProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SchedulerModule {

    @Provides
    @Singleton
    fun providesSchedulerSource(): BaseSchedulerProvider =
            SchedulerProvider.getInstance()

}
