package com.appetiser.mycodingchallenge.di

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.appetiser.android.baseplate.persistence.AppDatabase
import com.appetiser.mycodingchallenge.utils.Constants
import com.appetiser.mycodingchallenge.utils.PreferencesHelper
import dagger.Module
import dagger.Provides
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Singleton

@Module
class StorageModule {

    @Provides
    @Singleton
    fun providesSharedPreferences(application: Application): SharedPreferences {
        return PreferencesHelper.customPrefs(application.applicationContext, Constants.PREF_KEY)
    }

    @Provides
    @Singleton
    fun providesAppDatabase(application: Application): AppDatabase {
        return AppDatabase.getInstance(application.applicationContext)
    }
    @Singleton
    @Provides
    fun providesDateFormat(): SimpleDateFormat = SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH)

}
