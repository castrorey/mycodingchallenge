package com.appetiser.mycodingchallenge.di.builders

import com.appetiser.mycodingchallenge.di.scopes.FragmentScope
import com.appetiser.mycodingchallenge.features.itunes.trackdetails.TrackDetailsFragment
import com.appetiser.mycodingchallenge.features.itunes.tracklist.TrackListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeTrackListFragment() : TrackListFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeTrackDetailsFragment() : TrackDetailsFragment
}