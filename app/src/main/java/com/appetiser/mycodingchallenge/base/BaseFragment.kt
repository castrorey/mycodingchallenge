package  com.appetiser.mycodingchallenge.base

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.appetiser.mycodingchallenge.di.Injectable
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

abstract class BaseFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    protected lateinit var disposable: CompositeDisposable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        disposable = CompositeDisposable()
    }

    override fun onStop() {
        super.onStop()
        disposable.clear()
    }

    fun show(fragment: BaseFragment, content: Int, tag: String) {
        val transaction = fragmentManager?.beginTransaction()
        val prevFragment = fragmentManager?.findFragmentByTag(tag)
        if (prevFragment != null) {
            transaction?.remove(prevFragment)
        }
        transaction?.addToBackStack(null)
        transaction?.add(content, fragment, tag)?.commit()
    }
}
