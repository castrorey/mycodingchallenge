package com.appetiser.mycodingchallenge.base

interface BaseContract {
    fun showProgressLoading(visible: Boolean)
    fun showToast(message: String)
}
